# MicroService Stock Trading Platform Project

## Overview

The aim of this project is to create a stock trading platform which will perform the following core features:

1. A web UI should facilitate users to:
    * Place an order for a given Stock
    * Browse their trading history
    * View the status of each of their historical Trades

2. For the initial version there will be no authentication and a single user is assumed, i.e. there is no requirement to manage users in the initial revision.

3. You should use MySQL for any persistent storage.

4. You should aim to complete the core functionality described above before attempting any of the extensions described later in the document. The core technical elements should be designed by you. This document is provided as an outline, however all data storage, REST APIs etc. should be designed by your team.

## Suggested Schedule

A lot of the work for this system will take place during the afternoon sessions and during the 'Hackathon' time.

Each Friday you will be primarily tasked with addressing a particular area e.g Week 1: Java backend. However, from Week 3 onwards you have the option to work on any area of this project during the Friday hackathon. E.g. some of your team may continue work on a previously started component, or some of your team may (should) begin designing the user interface ahead of the final week.

The entire system is not expected to be complete until the final Friday.

### System Architecture
It is up to you and your team to decide on an overall architecture for the system. However, you should consider enterprise deployment environments and aim for a microservice style architecture.

The following architecture is the suggested architecture for the core components:

![Trade Project Diagram](https://www.benivade.com/neueda-training/Tech2020/CoreTradeProject2020.png)

1. Trade REST API: This should be written in Java as a spring boot REST application. It will accept HTTP REST requests to Create Orders & Retrieve Trade records from the TradeDB. Update and Delete of Trade records should be subject to sensible business considerations.

2. TradeDB: This should be data stored in a MySQL database. You do not need to deploy a production database, one will be provided for you.


3. Dummy Order/Trade Fullfillment Service: Sample source code for this component will be provided, which you will need to edit or update as required e.g. to match your specific MySQL schema. This service will read Order records from the "TradeDB" and simulate fullfilling or rejecting those Orders, it will mark the corresponding Order record with an appropriate status e.g. PROCESSING, FULLFILLED, REJECTED. The ideal solution would entail your team creating your own implementation of this component, this will allow your team to standout from the others.

4. Web Frontend: This is a web UI written in Javascript, Angular, or any other front-end framework you are experienced with (check with your instructor first). This UI should allow users to browse their Trading history, and request for Trades to be made by sending HTTP requests to the Trade REST API.

Note on terminology and limitations: For the purposes of this exercise there are limitatons to the functionality. The terms Order and Trade are largely interchangeable though in the real world they are differnt things. An order is an "intention to trade", e.g. I'd like to buy 1000 Tesla shares, or I'd like to sell 2500 Apple shares, while a trade is the confirmation "you bought 200 Tesla shares @ 500USD and 372 Tesla shares @501 USD, etc. As such, in a real scenario, a single order may result in many smaller separate trades at a range of prices. For this exercise all orders are considered as "immediate" or "Market" orders and will fill completely or not at all, and they will always fill at the current market price, there is no concept of "limit" orders and no orders will "rest" in the market.

### Optional Extensions

Once you have finished the core features, you may add some of the optional extras described below, or come up with your own extensions that you think would suit this system. Check with your instructors before beginning implementation of any extension features.

Some extra optional features you may add include:

    * A service that manages the users current portfolio, e.g. what Stocks, Cash, Bonds etc. they are currently holding.
    OR
    * A service that will give advice on suggested trades e.g. for a given stock should the user BUY, SELL or HOLD
    OR
    * A service that will automatically make trades based on user defined criteria e.g. the stock to trade, the maximum value of trades, when to stop making trades
    OR
    * A service that will integrate with Slack to give users updates on the status of trades

An example of how some of these components might fit within the system is shown below. However, these are SUGGESTIONS, the design of these elements should be driven by your team. You are definitely NOT expected to complete all of the elements shown below.

![Trade Project Diagram](https://www.benivade.com/neueda-training/Tech2020/ExtendedTradeProject2020.png)


5. Portfolio REST API: This could be written in Java and may even be part of the same spring boot application as the "Trade REST API". Alternatively, it may be a standalone spring boot application. This service should keep track of multiple asset classes (Cash, Stocks etc.) currently held.

6. PortfolioDB: Similar to TradeDB this should be data stored in a MySQL database.

7. Live Price Service REST API: If included, this may be written in Python or Java. This component would make price data available to your other components through a REST API. E.g. so that live price data could be shown in the UI. This component may read it's price data from a live service such as Yahoo Finance or elsewhere.

8. External Price Service: This is an External service e.g. Yahoo Finance.

9. Trade Advice REST API: If included, this may be written in Python OR Java. This service would receive RESTful requests for advice regarding stock tickers. It should return an answer indicating whether the advice is to BUY, SELL or HOLD that stock. E.g. The UI may use this service to indicate to the user what may be a sensible Trading option.

### Teamwork
It is expected that you work closely as a team during this project.

Your team should be self-organising, but should raise issues with instructors if they are potential blockers to progress. 

Your team should use a task management system such as Trello to keep track of tasks and progress.

Your team should keep track of all source code with git and bitbucket.

You may choose to create a separate bitbucket repository for each component that you tackle e.g. front-end code can be in its own repository. If you create more than one spring-boot application, then each can have its own bitbucket repository. To keep track of your repositories, you can use a single bitbucket 'Project' that each of your repositories is part of.

 Your instructor and team members need to access all repositories, so they should be either A) made public OR B) shared with your instructor and all team members.

Throughout your work, you should ensure good communication and organise regular check-ins with each other.



### Appendix A: Optional Extra - Portfolio REST API

This API would be used to keep track of a personal portfolio of cash and investments (e.g. stocks, bonds).

The API would facilitate CRUD operations for this data, however you may choose to only implement some of the standard CRUD operations given the available time.

Some example operations that would be facilitated by this API:
* Add some stocks for a particular ticker to your portfolio
* Calculate the users' stock holdings by querying the Trade history.
* Add some cash to your portfolio.
* Remove some cash from your portfolio
* Add other assets to your portfolio.
* Remove other assets from your portfolio


### Appendix B: UI Ideas

The screen below might give you some ideas about the type of UI that could be useful. You are NOT expected to implement the screen below exactly as it is shown. This is JUST FOR DEMONSTRATION of the type of thing that COULD be shown.

![Demonstration Portfolio UI](https://www.benivade.com/neueda-training/Tech2020/DemoPortfolioScreen.png)


### Appendix C: Some Useful Lambdas

There are some Python lambdas on the conygre training account that may be of use. You may use these directly, or copy and edit them according to your needs.

1. SimplePriceFeed: Gives a number of days of price data from yahoo finance using pandas data reader.

    * [Lambda Source](https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/simplePriceFeed)
    * [Endpoint URL](https://tdw7fepw1k.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=TSLA&num_days=10)


3. Trade Advisor: Uses a Bollinger Bands calculation to return advice of "Buy", "Sell" or "Hold" for a given stock ticker

    * [Lambda Source](https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/testTradeAdvice)
    * [Endpoint URL](https://i9261l04zh.execute-api.eu-west-1.amazonaws.com/default/testTradeAdvice?ticker=C)



